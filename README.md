# Capstone_Utilities
The idea of this project is to produce very robust classes which multiple projects across capstone need.
The intended use of this project is to provide source libraries to be copied into other projects.

If important improvements or changes that improve the class are discovered during development in another project,
please apply the changes in this repo so that the benefits are shared.  Keep in mind that if you change something that is
visible from the front end (ie changing existing function declarations or public variables), you need to mind backcompatibility
and try not to break other peoples code which depends on the current state of the software.  The two options for maintaining compatibility
are to branch with your modifications (and push to share), or make additional functions rather than changing existing ones.

Please make a minimal example executable when possible to show propper usage of your library (also for reproducing / testing min bugs).

Any library added to this project should have a "merge master" (listed below) who fields bug notes and merges in branch suggestions.

Merge Masters:
-exampleLib (developer)
-capSerial (Tom Lord)


Outstanding Bug reports:
-exampleLib (reporter): bug description